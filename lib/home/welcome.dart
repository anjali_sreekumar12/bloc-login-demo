import 'package:flutter/material.dart';
import 'package:ooptx_sample/common/SessionManager.dart';
class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  String name,email,contact;
  void setData() async{
    name = await SessionManager.getName();
    email = await SessionManager.getEmail();
    contact = await SessionManager.getContact();
    setState(() {

    });

  }
  @override
  void initState() {
    // TODO: implement initState
    setData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

      return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("${name}",style: TextStyle(
                  color: Colors.black,
                  fontSize: 20
                )),
                Text("${email}",style: TextStyle(
                    color: Colors.black,
                    fontSize: 20
                )),
                Text("${contact}",style: TextStyle(
                    color: Colors.black,
                    fontSize: 20
                )),
              ],
            )),
      );

  }
}
