import 'package:shared_preferences/shared_preferences.dart';
class SessionManager{

   static Future<bool> getIsSessionStarted() async{
     final prefs = await SharedPreferences.getInstance();
     final isSessionStarted = prefs.getBool('IsSessionStarted');
     if(isSessionStarted==null){
       return false;
     }
     return isSessionStarted;
   }
   static Future<int> getAdminID() async{
     final prefs = await SharedPreferences.getInstance();
     final adminid = prefs.getInt('AdminID');
     if(adminid==null){
       return 0;
     }
     return adminid;
   }
   static Future<String> getName() async{
     final prefs = await SharedPreferences.getInstance();
     final name = prefs.getString('Name');
     if(name==null){
       return '';
     }
     return name;
   }
   static Future<String> getEmail() async{
     final prefs = await SharedPreferences.getInstance();
     final name = prefs.getString('Email');
     if(name==null){
       return '';
     }
     return name;
   }
   static Future<String> getContact() async{
     final prefs = await SharedPreferences.getInstance();
     final name = prefs.getString('Contact');
     if(name==null){
       return '';
     }
     return name;
   }
   static Future<void> setUserCredentials(String id,String name, String email,String contact) async{
     final prefs = await SharedPreferences.getInstance();
     await prefs.setString('AdminID', id);
     await prefs.setString('Name', name);
     await prefs.setString('Email', email);
     await prefs.setString('Contact', contact);
   }
   static Future<void> setIsSessionStarted(bool flag) async{
     final prefs = await SharedPreferences.getInstance();
     await prefs.setBool('IsSessionStarted', flag);
   }
 }