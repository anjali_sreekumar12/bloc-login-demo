import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ooptx_sample/login/login_bloc.dart';
import 'package:ooptx_sample/login/login_repo.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ooptx_sample/home/welcome.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/welcome' : (context)=> Welcome()
      },
      home: Scaffold(
        backgroundColor: Colors.black,
        body: BlocProvider(
          create:(context) => LoginBloc(UserRepo(),context),
          child: LoginPage(),
        ),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final _loginbloc = BlocProvider.of<LoginBloc>(context);
    _loginbloc.add(ResetLoginEvent());
    return Center(

        child: BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              if (state is LoginButtonNotCicked) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 200,
                      height: 200,
                      child: Image(
                          image: AssetImage('assets/images/shopathome.jpg'),
                          fit: BoxFit.cover
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                        width: 300,
                        child: TextFormField(
                          controller: usernameController,
                          style: TextStyle(
                              color: Colors.white
                          ),
                          cursorColor: Colors.orange[900],
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    color: Colors.orange[900], width: 1.0),
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: BorderSide(color: Colors
                                    .orange[900]),
                              ),
                              labelText: 'Enter username',
                              labelStyle: TextStyle(
                                  color: Colors.orange[900]
                              )
                          ),
                          validator: (val) {
                            if (val.length == 0) {
                              return "Email cannot be empty";
                            } else {
                              return null;
                            }
                          },
                        )
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                        width: 300,
                        child: TextFormField(
                          controller: passwordController,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          obscureText: true,
                          cursorColor: Colors.orange[900],
                          decoration: InputDecoration(
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    color: Colors.orange[900], width: 1.0),
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: Colors.orange[900]),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white
                              ),
                              labelText: 'Enter password',
                              labelStyle: TextStyle(
                                  color: Colors.orange[900]
                              )
                          ),
                          validator: (val) {
                            if (val.length == 0) {
                              return "Email cannot be empty";
                            } else {
                              return null;
                            }
                          },)
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 300,
                      color: Colors.orange[900],
                      child: FlatButton(
                        onPressed: () {
                          _loginbloc.add(LoginUserEvent(usernameController.text,
                              passwordController.text));
                        },
                        color: Colors.orange[900],
                        child: Text(
                          'Login',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else if (state is LoginButtonClicked) {
                return Container(
                  color: Colors.white,
                  child: Center(
                    child: SpinKitCubeGrid(color: Colors.orange[900], size: 80),
                  ),
                );
              } else if (state is LoginSuccess) {
              //  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Welcome()));
                return Container(
                  color: Colors.white,
                  child: Center(
                    child: Text('Loggedin..'),
                  ),
                );
              }
              else {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 200,
                      height: 200,
                      child: Image(
                          image: AssetImage('assets/images/shopathome.jpg'),
                          fit: BoxFit.cover
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                        width: 300,
                        child: TextFormField(
                          controller: usernameController,
                          style: TextStyle(
                              color: Colors.white
                          ),
                          cursorColor: Colors.orange[900],
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    color: Colors.orange[900], width: 1.0),
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: BorderSide(color: Colors
                                    .orange[900]),
                              ),
                              labelText: 'Enter username',
                              labelStyle: TextStyle(
                                  color: Colors.orange[900]
                              )
                          ),
                          validator: (val) {
                            if (val.length == 0) {
                              return "Email cannot be empty";
                            } else {
                              return null;
                            }
                          },
                        )
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                        width: 300,
                        child: TextFormField(
                          controller: passwordController,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          obscureText: true,
                          cursorColor: Colors.orange[900],
                          decoration: InputDecoration(
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25),
                                borderSide: BorderSide(
                                    color: Colors.orange[900], width: 1.0),
                              ),
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: Colors.orange[900]),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.white
                              ),
                              labelText: 'Enter password',
                              labelStyle: TextStyle(
                                  color: Colors.orange[900]
                              )
                          ),
                          validator: (val) {
                            if (val.length == 0) {
                              return "Email cannot be empty";
                            } else {
                              return null;
                            }
                          },)
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 300,
                      color: Colors.orange[900],
                      child: FlatButton(
                        onPressed: () {
                          _loginbloc.add(LoginUserEvent(usernameController.text,
                              passwordController.text));
                        },
                        color: Colors.orange[900],
                        child: Text(
                          'Login',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      child: Center(
                        child: Text(
                          'Login Failed!',
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
            }

        )
    );
  }
}


