import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:ooptx_sample/login/User.dart';
import 'package:ooptx_sample/login/login_repo.dart';
import 'package:ooptx_sample/common/SessionManager.dart';
import 'package:ooptx_sample/home/welcome.dart';

// declaring event class for login
class LoginEvent extends Equatable{

  @override
  // TODO: implement props
  List<Object> get props => [];

}
// creating first event (on login button clicked)
class LoginUserEvent extends LoginEvent{
  String username,password;
  LoginUserEvent(this.username,this.password);

  @override
  // TODO: implement props
  List<Object> get props => [username,password];
}

class ResetLoginEvent extends LoginEvent{
   ResetLoginEvent();
   @override
  // TODO: implement props
  List<Object> get props => [];
}

//declaring state class for Login
class LoginState extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

//declaring different type of login states

//first state
class LoginButtonNotCicked extends LoginState{

}
// button clicked and waiting for login
class LoginButtonClicked extends LoginState{

}

class LoginSuccess extends LoginState{

}
void setSessions (User user) async{
  SessionManager.setUserCredentials(user.userid,user.username,user.email,user.contact);
      SessionManager.setIsSessionStarted(true);

}
class LoginFailed extends LoginState{

}
// Defining LoginBloc
class LoginBloc extends Bloc<LoginEvent,LoginState>{
  BuildContext context;
  UserRepo userRepo;
  //constructor
  LoginBloc(this.userRepo,this.context) : super(null);
  //return initial state
  @override
  LoginState get initialState => LoginButtonNotCicked();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async*{
    // TODO: implement mapEventToState
    if(event is LoginUserEvent){
      yield LoginButtonClicked();
      try{
        User user = await userRepo.login(event.username, event.password);
        if(user.status=="true") {
          yield LoginSuccess();

          setSessions(user);
          Route route = MaterialPageRoute(builder: (context) => Welcome());
          Navigator.push(context, route);
        }else{
          yield LoginFailed();
        }
      }catch(_){
        yield LoginFailed();
      }
    }else{
      yield LoginButtonNotCicked();
    }
  }

}

