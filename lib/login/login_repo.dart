import 'dart:convert';
//import 'dart:io';
import 'package:http/http.dart' as http;
//import 'package:ooptx_sample/common/SessionManager.dart';
import 'package:ooptx_sample/common/api_url.dart';
import 'package:ooptx_sample/login/User.dart';
class UserRepo {

  //api call for login, return User object
  Future<User> login(String username,String password) async {

    //input parameters username and password
    Map data = {
      'username': username,
      'password':password
    };

    // encode inut prameters to json
    String body = json.encode(data);

    //network reuest for login
    http.Response response = await http.post(
      Utils().api_url+Utils().login,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: body,
    );

    //check if the api call is successful
    if (response.statusCode == 200) {
      //save the result as map object
      Map result = jsonDecode(response.body);
      print(result.toString());
     return User.fromJson(result);
    }else {
      throw Exception;
    }
  }

}