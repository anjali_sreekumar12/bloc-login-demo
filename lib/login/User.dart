class User{
  //parameters
  String name,email,contact;
  String userid,status;


  //constructor
  User(this.name,this.email,this.contact,this.userid,this.status);
  // getters for the class parameters
  String get username => name;
  String get useremail => email;
  String get usercontact => contact;
  String get id =>userid;
  String get loginstatus =>status;

  //method to convert json response from api to User object
  factory User.fromJson(Map<String,dynamic> json){
    return User(
        json["name"] as String,
        json["email"] as String,
        json["contact"] as String,
        json["userid"] as String,
        json["status"] as String
    );
  }
}